## Benvenuti!

Ciao a tutti, noi siamo la squadra "I TUXXINI" del Liceo Scientifico [Rainerum](https://www.rainerum.it/ "School Website") di Bolzano.
Questa è la nostra pagina web, creata con [GitLab + Hugo](https://gohugo.io "Hugo Page").

Per leggere di più sulla nostra squadra, visita la sezione [About](/page/about).

In fondo alla pagina e nella sezione "SOCIAL MEDIA" in alto a destra potete trovare i canali social della squadra.
