---
title: Il nostro progetto
date: 2021-03-08
bigimg: [{src: "/img/sphere.jpg"}]
---

{{< youtube id="T3MdWX4_owU" >}}

Questo video è stato realizzato per esporre la nostra idea e come intendiamo realizzarla.
La voce è di Samuele, mentre il video è stato editato da Davide.
Il video si può trovare sul nostro [canale YouTube](https://www.youtube.com/channel/UClKC7Y-IXTgcg5dYJvOBkxQ).

This video has been produced to explain our idea and how we intend to realize it.
The voice is of Samuele, while the video has been edited by Davide.
The video can be found on our [YouTube channel](https://www.youtube.com/channel/UClKC7Y-IXTgcg5dYJvOBkxQ).
